#!/usr/bin/env python3
# -*- coding=utf-8 -*-
# 本脚由亁颐堂现任明教教主编写，用于乾颐盾Python课程！
# 教主QQ:605658506
# 亁颐堂官网www.qytang.com
# 教主技术进化论拓展你的技术新边疆
# https://ke.qq.com/course/271956?tuin=24199d8a

# 为与经典协议的WIN10
from GET import snmpv2_get
import time
from datetime import datetime
import requests


while True:
    cpu_percent = int(snmpv2_get("10.1.1.253", "qytangro", "1.3.6.1.4.1.9.9.109.1.1.1.1.3.7", port=161)[1])
    router_name = 'R1'
    cpu_timestamp = str(datetime.now().timestamp())
    post_dict = {'cpu_percent': cpu_percent, 'router_name': router_name, 'cpu_timestamp': cpu_timestamp}
    print(post_dict)
    result = requests.post('https://api.mingjiao.org/api/cpu-usage', json=post_dict)
    print(result.json())
    time.sleep(10)
