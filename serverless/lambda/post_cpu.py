# serverless_restapi_create
import json
import logging
import uuid
from datetime import datetime

import boto3

dynamodb = boto3.resource('dynamodb')


def post_cpu(event, context):
    data = json.loads(event['body'])
    if 'router_name' not in data:
        logging.error("Validation Failed")
        raise Exception("Missing router_name")

    if 'cpu_percent' not in data:
        logging.error("Validation Failed")
        raise Exception("Missing cpu_percent")

    if 'cpu_timestamp' not in data:
        logging.error("Validation Failed")
        raise Exception("Missing cpu_timestamp")

    rcv_timestamp = str(datetime.utcnow().timestamp())
    router_name = data['router_name']
    cpu_percent = data['cpu_percent']
    cpu_timestamp = str(data['cpu_timestamp'])

    table = dynamodb.Table('qyt_serverless_table')

    item = {'id': str(uuid.uuid1()), 'router_name': router_name, 'cpu_percent': cpu_percent,
            'cpu_timestamp': cpu_timestamp, 'rcv_timestamp': rcv_timestamp}

    # write the todo to the database
    table.put_item(Item=item)

    # create a response
    response = {"statusCode": 200, "body": json.dumps(item)}

    return response