import decimal
import json
import boto3
from boto3.dynamodb.conditions import Key, And
from datetime import datetime
import time


# This is a workaround for: http://bugs.python.org/issue16535
class DecimalEncoder(json.JSONEncoder):
    def default(self, obj):
        if isinstance(obj, decimal.Decimal):
            return int(obj)
        return super(DecimalEncoder, self).default(obj)


dynamodb = boto3.resource('dynamodb')


def line_data(name, time_speed_list, color):
    return {'symbolSize': 0,  # 这个参数表示在图像上显示的原点大小，为0则不显示
            'symbol': 'circle',
            'name': name,
            'type': 'line',
            'smooth': True,
            'smoothMonotone': True,
            'data': time_speed_list,
            'areaStyle': {'color': color},
            'markPoint': {'itemStyle': {'color': color},
                          'data': [{'type': 'max', 'name': '最大值'},
                                   {'type': 'min', 'name': '最小值'}]},
            'lineStyle': {'color': color},
            'itemStyle': {'color': color}}


def get_cpu_list(event, context):
    table = dynamodb.Table('qyt_serverless_table')

    # fetch all todos from the database
    result = table.scan(FilterExpression=Key("router_name").eq(event['pathParameters']['router-name']))

    cpu_time_list = []
    # type_1 = type(json.dumps(result['Items'], cls=DecimalEncoder))
    for x in result['Items']:
        cpu_time_list.append([datetime.fromtimestamp(float(x["cpu_timestamp"])), int(x["cpu_percent"])])

    cpu_time_list_sorted = sorted(cpu_time_list, key=lambda x: x[0])
    cpu_time_list_converted = [[int(time.mktime(x[0].timetuple())) * 1000, x[1]] for x in cpu_time_list_sorted]

    cpu_datas = [line_data('R1 CPU利用率', cpu_time_list_converted, '#00BFFF')]

    json_data = {'labelname': 'CPU利用率',
                 'legends': [x['name'] for x in cpu_datas],
                 'datas': cpu_datas,
                 'starttime': '2019-12-29'}
    response = {"statusCode": 200, "headers": {"Access-Control-Allow-Origin": "*"}, "body": json.dumps(json_data)}

    return response
