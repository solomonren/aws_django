#!/usr/bin/env python3
# -*- coding=utf-8 -*-
# 本脚由亁颐堂现任明教教主编写，用于乾颐盾Python课程！
# 教主QQ:605658506
# 亁颐堂官网www.qytang.com
# 教主技术进化论拓展你的技术新边疆
# https://ke.qq.com/course/271956?tuin=24199d8a
import django
import os
os.environ.setdefault('DJANGO_SETTINGS_MODULE', 'aws_django.settings')
django.setup()
from qytdb.models import Department

d1 = Department(name='Security',
                summary='QYTANG Security CCIE',
                teacher='cq_bomb',
                method='online',
                characteristic='hacker and other vender fw',
                if_provide_lab=True,
                detail='cq_bomb network security')

d1.save()

d2 = Department(name='EI',
                summary='QYTANG EI CCIE',
                teacher='ender',
                method='online',
                characteristic='sd-wan, sdn',
                if_provide_lab=True,
                detail='ender EI CCIE')

d2.save()


if __name__ == '__main__':
    pass