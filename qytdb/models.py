from django.db import models


# Create your models here.
class Department(models.Model):
    name = models.CharField(max_length=100, blank=False, null=False)
    summary = models.CharField(max_length=10000, blank=True, null=True)
    teacher = models.CharField(max_length=100, blank=False)
    method = models.CharField(max_length=100, blank=False)
    characteristic = models.CharField(max_length=100, blank=True, null=True)
    if_provide_lab = models.BooleanField(default=True)
    detail = models.CharField(max_length=10000, blank=False)

    def __str__(self):
        return f"{self.__class__.__name__}(Name: {self.name} | Teacher: {self.teacher})"
