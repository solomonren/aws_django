#!/bin/bash
# 安装python3
yum install -y python36 python36-virtualenv python36-pip git
ln -s /usr/bin/pip-3.6 /usr/bin/pip3

# 激活ssh root密码登录
sed -i "s/PasswordAuthentication no/PasswordAuthentication yes/g" /etc/ssh/sshd_config
sed -i "s/#PermitRootLogin yes/PermitRootLogin yes/g" /etc/ssh/sshd_config
sed -i "s/UsePAM yes/UsePAM no/g" /etc/ssh/sshd_config
> ~/.ssh/authorized_keys
sudo sh -c 'echo root:Cisc0123 | chpasswd'
service sshd restart

# 加载EFS
yum install -y amazon-efs-utils
mkdir /efs_django
sudo mount -t efs fs-0f94df6e:/ /efs_django
# 自动启动
# /etc/fstab
fs-0f94df6e:/ /efs_django efs defaults,_netdev 0 0
df -h

# 初始化数据库
yum install -y postgresql
psql -h qytang-database-cluster.cluster-cfuddm7sqi1u.ap-northeast-2.rds.amazonaws.com -U postgres
postgres=> create database qytangdb;
CREATE DATABASE
postgres=> create user qytangdbuser;
CREATE ROLE
postgres=> \password qytangdbuser;
Enter new password:
Enter it again:
postgres=> grant all on database qytangdb to qytangdbuser;
postgres=> \q

# 安装python 模块
pip3 install boto3
pip3 install requests
pip3 install django
pip3 install psycopg2-binary
pip3 install django_redis

# 修改setting.py[在本地修改!git到gitee] (rds域名和redis域名)

# git下载项目
cd /efs_django
yum install -y git
git clone https://gitee.com/qytang/aws_django.git


python3 manage.py makemigrations
python3 manage.py migrate
python3 manage.py shell # 添加rds数据库数据
# 执行 insert_psql.py内的代码
# 测试redis
from django.core.cache import cache
cache.set('v', '555', 60*60)
cache.get('v')


# 使用python脚本创建数据库表
python3 /efs_django/aws_django/modules/insert_db_1_create_table.py
# 使用python脚本向数据库接入数据
python3 /efs_django/aws_django/modules/insert_db_2_insert.py


# 安装与配置uwsgi
yum install -y gcc
pip3 install uwsgi
cp /efs_django/aws_django/configure_script/uwsgi.conf /etc/init/uwsgi.conf
start uwsgi #/etc/rc.local

# 安装与配置NGINX
yum install -y nginx
mv /etc/nginx/nginx.conf /etc/nginx/nginx.conf-bak
cp /efs_django/aws_django/configure_script/nginx.conf /etc/nginx/nginx.conf
service nginx start
chkconfig nginx on