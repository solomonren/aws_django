#!/usr/bin/env python3
# -*- coding=utf-8 -*-
# 本脚由亁颐堂现任明教教主编写，用于乾颐盾Python课程！
# 教主QQ:605658506
# 亁颐堂官网www.qytang.com
# 教主技术进化论拓展你的技术新边疆
# https://ke.qq.com/course/271956?tuin=24199d8a

from django.shortcuts import render
import requests


def index(request):
    instance_id = requests.get("http://169.254.169.254/latest/meta-data/instance-id").text
    availability_zone = requests.get("http://169.254.169.254/latest/meta-data/placement/availability-zone").text

    return render(request, 'index.html', {'instance_id': instance_id,
                                          'availability_zone': availability_zone,
                                          'active': '首页'})
