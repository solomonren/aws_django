#!/usr/bin/env python3
# -*- coding=utf-8 -*-
# 本脚由亁颐堂现任明教教主编写，用于乾颐盾Python课程！
# 教主QQ:605658506
# 亁颐堂官网www.qytang.com
# 教主技术进化论拓展你的技术新边疆
# https://ke.qq.com/course/271956?tuin=24199d8a

from django.shortcuts import render
from datetime import datetime
from qytdb.models import Department
from django.views.decorators.cache import cache_page


@cache_page(600)
def course_info(request):
    qytsummary = 'QYTANG课程摘要'
    mytime = int(datetime.strptime('2019-12-2', '%Y-%m-%d').strftime("%w"))
    course_list = [course.name for course in Department.objects.all()]
    teacher_list = [{'course': course.name, 'teacher': course.teacher} for course in Department.objects.all()]
    return render(request, 'course_info.html',
                  {'qytsummary': qytsummary,
                   'course_list': course_list,
                   'teacher_list': teacher_list,
                   'active': '课程信息(PSQL)',
                   'mytime': mytime})
