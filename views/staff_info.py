#!/usr/bin/env python3
# -*- coding=utf-8 -*-
# 本脚由亁颐堂现任明教教主编写，用于乾颐盾Python课程！
# 教主QQ:605658506
# 亁颐堂官网www.qytang.com
# 教主技术进化论拓展你的技术新边疆
# https://ke.qq.com/course/271956?tuin=24199d8a

from django.shortcuts import render
from modules.insert_db_3_get_item import get_all_item


def staff_info(request):
    staff_list = get_all_item('staff')
    return render(request, 'staff_info.html', {'staff_list': staff_list, 'active': '员工信息(DynamoDB)'})
